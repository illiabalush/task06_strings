package model.textobject;

import interfaces.Textable;

import java.util.Objects;
import java.util.regex.Pattern;

public class Digit implements Textable {
    private String number;
    private Pattern pattern;

    public Digit() {
        number = "";
        pattern = Pattern.compile("[0-9]+");
    }

    public Digit(String number) {
        this();
        this.number = number;
    }

    @Override
    public Pattern getPattern() {
        return pattern;
    }

    @Override
    public String getText() {
        return number;
    }

    @Override
    public void setText(String text) {
        this.number = text;
    }

    @Override
    public Textable getInstance(String text) {
        return new Digit(text);
    }

    @Override
    public int compareTo(Textable o) {
        return o.getText().compareTo(number);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Digit digit = (Digit) o;
        return Objects.equals(number, digit.number);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    @Override
    public String toString() {
        return number;
    }
}
