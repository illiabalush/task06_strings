package view;

import interfaces.Printable;
import interfaces.Textable;
import model.textobject.Sentence;

import java.util.*;

public class ConsoleView extends View {
    private Scanner scanner;
    private Map<String, String> menu;
    private Map<String, Printable> menuItems;

    public ConsoleView() {
        scanner = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - find a sentence with the largest number of identical words");
        menu.put("2", "2 - sort sentences in the order of increasing number of words");
        menu.put("3", "3 - find the words contained in the first sentence only");
        menu.put("4", "4 - in the questionable sentences find and print without repetition words of a given length");
        menu.put("5", "5 - in each sentence swap a longest words with word on a vowel letter");
        menu.put("6", "6 - print all words in alphabetical order");
        menu.put("7", "7 - sort all words in the order of increasing percentage of a vowel letters");
        menu.put("8", "8 - words that begins on a vowel letter sort by a first consonant letter");
        menu.put("9", "9 - sort all word in order of increasing number of a given letter");
        menu.put("10", "10 - for each words from list of words find how many times it occurs in text and sort" +
                "list in order of decreasing total number of occurrences");
        menu.put("11", "11 - for each sentence remove a substring of a maximum length which begin and ends " +
                "of a given letters");
        menu.put("12", "12 - remove all words of a given length that beginning on a consonant letter");
        menu.put("13", "13 - sort words in the order of decreasing number of a given letter");
        menu.put("14", "14 - find a longest palindrome");
        menu.put("15", "15 - modify each word removing from it all subsequent (previous) entry of " +
                "the first (last) letters of this word.");
        menu.put("16", "16 - in a specific sentence all words of a given length change to a given substring");
        menu.put("17", "17 - show text");
        menu.put("Q", "Q - exit");
        menuItems = new LinkedHashMap<>();
        menuItems.put("1", this::showSentenceWithBiggestNumberIdenticalWords);
        menuItems.put("2", this::showSentencesSortedByWordsNumber);
        menuItems.put("3", this::showWordsContainsOnlyInFirstSentence);
        menuItems.put("4", this::showWordsFixedLengthWithoutRepeatInQuestionableSentence);
        menuItems.put("5", this::showSentencesWithSwappedLongestAndVowelWords);
        menuItems.put("6", this::showSortedTextByFirstLetter);
        menuItems.put("7", this::showSortedWordsByPercentageVowelLetters);
        menuItems.put("8", this::showVowelWordsSortedByConsonant);
        menuItems.put("9", this::showWordsSortedByNumberOfFixedLetter);
        menuItems.put("10", this::showSortedFixedListByNumberOfWordsInText);
        menuItems.put("11", this::showCuttedSentences);
        menuItems.put("12", this::showSentencesAfterDeletingFixedConsonantWords);
        menuItems.put("13", this::showWordsSortedByNumberOfFixedLetterReversed);
        menuItems.put("14", this::showLongestPalindrome);
        menuItems.put("15", this::showModifiedWordsByDeletingLetters);
        menuItems.put("16", this::showChangedFixedLengthToFixedString);
        menuItems.put("17", this::showText);
    }

    private void showAllSentences(List<Sentence> listOfSentences) {
        StringBuilder stringBuilder = new StringBuilder();
        listOfSentences.forEach(sentence -> {
            sentence.getSentence().forEach(textable -> stringBuilder.append(textable).append(" "));
            stringBuilder.append("\n");
        });
        logger.info(stringBuilder.toString());
    }

    private void showAllWords(List<Textable> listOfWords) {
        StringBuilder stringBuilder = new StringBuilder();
        listOfWords.forEach(textable -> stringBuilder.append(textable).append(" "));
        logger.info(stringBuilder.toString());
    }

    private void showSentenceWithBiggestNumberIdenticalWords() {
        Sentence sentence = controller.getModel().getSentenceWithBiggestNumberIdenticalWords();
        showAllWords(sentence.getSentence());
    }

    private void showSentencesSortedByWordsNumber() {
        List<Sentence> sentenceList = controller.getModel().getSentencesSortedByWordsNumber();
        showAllSentences(sentenceList);
    }

    private void showWordsContainsOnlyInFirstSentence() {
        List<Textable> textableList = controller.getModel().getWordsContainsOnlyInFirstSentence();
        showAllWords(textableList);
    }

    private void showWordsFixedLengthWithoutRepeatInQuestionableSentence() {
        logger.info("Enter word length");
        int wordLength = scanner.nextInt();
        List<Textable> wordsList = controller.getModel()
                .getWordsFixedLengthWithoutRepeatInQuestionableSentence(wordLength);
        showAllWords(wordsList);
    }

    private void showSentencesWithSwappedLongestAndVowelWords() {
        List<Sentence> sentenceList = controller.getModel().getSentencesWithSwappedLongestAndVowelWords();
        showAllSentences(sentenceList);
    }

    private void showSortedTextByFirstLetter() {
        List<Textable> textableList = controller.getModel().getSortedTextByFirstLetter();
        showAllWords(textableList);
    }

    private void showSortedWordsByPercentageVowelLetters() {
        List<Textable> textableList = controller.getModel().getSortedWordsByPercentageVowelLetters();
        showAllWords(textableList);
    }

    private void showVowelWordsSortedByConsonant() {
        List<Textable> textableList = controller.getModel().getVowelWordsSortedByConsonant();
        showAllWords(textableList);
    }

    private void showWordsSortedByNumberOfFixedLetter() {
        logger.info("Enter a character: ");
        String character = scanner.next();
        List<Textable> textableList = controller.getModel().getWordsSortedByNumberOfFixedLetter(character.charAt(0));
        showAllWords(textableList);
    }

    private void showSortedFixedListByNumberOfWordsInText() {
        List<String> fixedList = new ArrayList<>();
        logger.info("Enter your list of words: ");
        logger.info("Enter Q for exit!");
        String string = "";
        while (!string.toUpperCase().equals("Q")) {
            logger.info("Enter string: ");
            string = scanner.next();
            if (!string.trim().equals("") && !string.toUpperCase().equals("Q")) {
                fixedList.add(string);
            }
        }
        List<Textable> textableList = controller.getModel().getSortedFixedListByNumberOfWordsInText(fixedList);
        showAllWords(textableList);
    }

    private void showCuttedSentences() {
        logger.info("Enter begin of cutting: ");
        String start = scanner.next();
        logger.info("Enter end of cutting: ");
        String end = scanner.next();
        List<Sentence> sentenceList = controller.getModel().getCuttedSentences(start.charAt(0), end.charAt(0));
        showAllSentences(sentenceList);
    }

    private void showSentencesAfterDeletingFixedConsonantWords() {
        logger.info("Enter length: ");
        int wordLength = scanner.nextInt();
        List<Sentence> sentenceList = controller.getModel().getSentencesAfterDeletingFixedConsonantWords(wordLength);
        showAllSentences(sentenceList);
    }

    private void showWordsSortedByNumberOfFixedLetterReversed() {
        logger.info("Enter a letter: ");
        String character = scanner.next();
        List<Textable> textableList = controller.getModel()
                .getWordsSortedByNumberOfFixedLetterReversed(character.charAt(0));
        showAllWords(textableList);
    }

    private void showLongestPalindrome() {
        logger.info(controller.getModel().getLongestPalindrome());
    }

    private void showModifiedWordsByDeletingLetters() {
        List<Textable> textableList = controller.getModel().modifyWordsByDeletingLetters();
        showAllWords(textableList);
    }

    private void showChangedFixedLengthToFixedString() {
        logger.info("Enter sentence number: ");
        int indexSentence = scanner.nextInt();
        logger.info("Enter word length: ");
        int wordLength = scanner.nextInt();
        logger.info("Enter new string: ");
        scanner = new Scanner(System.in);
        String string = scanner.nextLine();
        Sentence sentence = controller.getModel()
                .getChangedFixedLengthToFixedString(indexSentence, wordLength, string);
        showAllWords(sentence.getSentence());
    }

    private void showText() {
        showAllSentences(controller.getModel().getText().getSentenceList());
    }

    @Override
    public void start() {
        String keyMenu;
        do {
            showMenu();
            scanner = new Scanner(System.in);
            logger.info("Please, select menu point:");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                menuItems.get(keyMenu).print();
            } catch (Exception ignored) {

            }
        } while (!keyMenu.equals("Q"));
    }

    @Override
    public void showMenu() {
        logger.info("\n");
        for (String i : menu.values()) {
            logger.info(i);
        }
    }
}
