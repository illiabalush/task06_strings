package controller;

import interfaces.Textable;
import model.*;
import model.textobject.Mark;
import model.textobject.Digit;
import model.textobject.Sentence;
import model.textobject.Word;
import view.View;

import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller {
    private Model model;
    private String textFromFile;
    private Textable[] sentenceObjects = {new Mark(), new Word(), new Digit()};

    public Controller() {
        model = new Model();
    }

    public void loadTextFromFile(String path) {
        StringBuilder stringBuilder = new StringBuilder();
        try (FileReader reader = new FileReader(path)) {
            int symbol;
            while ((symbol = reader.read()) != -1) {
                stringBuilder.append((char) symbol);
            }
        } catch (IOException ex) {
            View.showException(ex);
        }
        setTextFromFile(stringBuilder.toString());
    }

    public void parseTextFromFile() {
        Sentence sentence = new Sentence();
        StringBuilder commonPattern = new StringBuilder();
        for (Textable textable : sentenceObjects) {
            commonPattern.append(textable.getPattern().pattern()).append("|");
        }
        Matcher matcher = Pattern.compile(commonPattern.substring(0, commonPattern.length() - 1))
                .matcher(textFromFile);
        while (matcher.find()) {
            String matchString = textFromFile.substring(matcher.start(), matcher.end());
            sentence.addSentenceObjectToSentence(getInstanceOfSentenceObject(matchString));
            if (isEndSentence(matchString)) {
                model.getText().addSentenceToText(sentence);
                sentence = new Sentence();
            }
        }
    }

    private boolean isEndSentence(String text) {
        return text.matches("[!?.]");
    }

    private Textable getInstanceOfSentenceObject(String matchString) {
        for (Textable i : sentenceObjects) {
            if (matchString.matches(i.getPattern().pattern())) {
                return i.getInstance(matchString);
            }
        }
        return null;
    }

    public Model getModel() {
        return model;
    }

    public void setTextFromFile(String textFromFile) {
        this.textFromFile = textFromFile;
    }

    public String getTextFromFile() {
        return textFromFile;
    }
}
