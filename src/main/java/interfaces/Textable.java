package interfaces;

import java.util.regex.Pattern;

public interface Textable extends Comparable<Textable> {
    Pattern getPattern();
    String getText();
    Textable getInstance(String text);
    void setText(String text);
}
