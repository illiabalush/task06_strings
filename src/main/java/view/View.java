package view;

import controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class View {
    protected Controller controller;
    protected static Logger logger = LogManager.getLogger(View.class);

    public View() {
        controller = new Controller();
        controller.loadTextFromFile("C:\\Users\\User\\IdeaProjects\\task06_Strings\\src\\main\\resources\\text");
        controller.parseTextFromFile();
    }

    public abstract void start();

    public abstract void showMenu();

    public static void showException(Exception exception) {
        logger.error(exception);
    }
}
