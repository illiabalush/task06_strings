package model.textobject;

import interfaces.Textable;

import java.util.Objects;
import java.util.regex.Pattern;

public class Mark implements Textable {
    private String mark;
    private Pattern pattern;

    public Mark() {
        mark = "";
        pattern = Pattern.compile("[.,\\-?!:;\"]");
    }

    public Mark(String mark) {
        this();
        this.mark = mark;
    }

    @Override
    public Pattern getPattern() {
        return pattern;
    }

    @Override
    public String getText() {
        return mark;
    }

    @Override
    public void setText(String text) {
        this.mark = text;
    }

    @Override
    public Textable getInstance(String text) {
        return new Mark(text);
    }

    @Override
    public int compareTo(Textable o) {
        return o.getText().compareTo(mark);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mark mark1 = (Mark) o;
        return Objects.equals(mark, mark1.mark);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mark);
    }

    @Override
    public String toString() {
        return mark;
    }
}
