package model.textobject;

import java.util.ArrayList;
import java.util.List;

public class Text {
    private List<Sentence> sentenceList;

    public Text() {
        sentenceList = new ArrayList<>();
    }

    public void addSentenceToText(Sentence sentence) {
        sentenceList.add(sentence);
    }

    public List<Sentence> getSentenceList() {
        return sentenceList;
    }
}
