package model.textobject;

import interfaces.Textable;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class Sentence {

    private List<Textable> sentence;
    private Pattern patternSentence = Pattern.compile("([A-Z]+[.?!])");

    public Sentence() {
        sentence = new ArrayList<>();
    }

    public void addSentenceObjectToSentence(Textable sentenceObject) {
        sentence.add(sentenceObject);
    }

    public Pattern getPatternSentence() {
        return patternSentence;
    }

    public void setSentence(List<Textable> sentence) {
        this.sentence = sentence;
    }

    public List<Textable> getSentence() {
        return sentence;
    }
}
