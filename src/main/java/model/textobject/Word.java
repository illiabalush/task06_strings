package model.textobject;

import interfaces.Textable;

import java.util.Objects;
import java.util.regex.Pattern;

public class Word implements Textable {
    private String word;
    private Pattern pattern;

    public Word() {
        word = "";
        pattern = Pattern.compile("[A-Za-z]+");
    }

    public Word(String word) {
        this();
        this.word = word;
    }

    @Override
    public Pattern getPattern() {
        return pattern;
    }

    @Override
    public String getText() {
        return word;
    }

    @Override
    public void setText(String text) {
        this.word = text;
    }

    @Override
    public Textable getInstance(String text) {
        return new Word(text);
    }

    @Override
    public int compareTo(Textable o) {
        return word.toLowerCase().compareTo(o.getText().toLowerCase());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Word word1 = (Word) o;
        return Objects.equals(word.toLowerCase(), word1.word.toLowerCase());
    }

    @Override
    public int hashCode() {
        return Objects.hash(word);
    }

    @Override
    public String toString() {
        return word;
    }
}
