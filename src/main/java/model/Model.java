package model;

import interfaces.Textable;
import model.textobject.*;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Model {
    private Text text;
    private String wordPattern;

    public Model() {
        text = new Text();
        wordPattern = new Word().getPattern().pattern();
    }

    public Sentence getSentenceWithBiggestNumberIdenticalWords() {
        Map<Sentence, Integer> sentencesWithNumberOfIdenticalWords = getSentencesWithNumberOfIdenticalWords();
        return getSentenceByValue(sentencesWithNumberOfIdenticalWords,
                getBiggestNumberIdenticalWords(sentencesWithNumberOfIdenticalWords));
    }

    private Map<Sentence, Integer> getSentencesWithNumberOfIdenticalWords() {
        // 1 - filter only words
        // 2 - make all words to a lower case
        // 3 - grouping data to a map [word : number of this word in sentence]
        // 4 - take values[number of identical words] and search max number
        // 5 - put this sentence and max number to sentencesWithNumberOfIdenticalWords
        Map<Sentence, Integer> sentencesWithNumberOfIdenticalWords = new HashMap<>();
        text.getSentenceList().forEach(sentence -> sentencesWithNumberOfIdenticalWords.put(sentence,
                sentence.getSentence().stream().filter(textable -> textable.getText().matches(wordPattern))
                        .map(textable -> new Word(textable.getText().toLowerCase()))
                        .collect(Collectors.groupingBy(Textable::getText, Collectors.counting()))
                        .values().stream().max(Long::compareTo).get().intValue()));
        return sentencesWithNumberOfIdenticalWords;
    }

    private Sentence getSentenceByValue(Map<Sentence, Integer> identicalWords, Integer biggestValue) {
        for (Map.Entry<Sentence, Integer> map : identicalWords.entrySet()) {
            if (map.getValue().equals(biggestValue)) {
                return map.getKey();
            }
        }
        return null;
    }

    private Integer getBiggestNumberIdenticalWords(Map<Sentence, Integer> identicalWords) {
        Integer biggestNumberIdenticalWords = 0;
        for (Integer i : identicalWords.values()) {
            if (i > biggestNumberIdenticalWords) {
                biggestNumberIdenticalWords = i;
            }
        }
        return biggestNumberIdenticalWords;
    }

    public List<Sentence> getSentencesSortedByWordsNumber() {
        List<Sentence> sortedSentences = new ArrayList<>();
        Map<Sentence, Integer> sentencesWithNumberOfWords = getSentencesWithNumberOfWords();
        sentencesWithNumberOfWords.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .forEach(sentenceIntegerEntry -> sortedSentences.add(sentenceIntegerEntry.getKey()));
        return sortedSentences;
    }

    private Map<Sentence, Integer> getSentencesWithNumberOfWords() {
        Map<Sentence, Integer> sentencesWithNumberOfWords = new HashMap<>();
        text.getSentenceList().forEach(sentence -> sentencesWithNumberOfWords.put(sentence,
                (int) sentence.getSentence().stream().filter(textable -> textable.getText()
                        .matches(wordPattern)).count()));
        return sentencesWithNumberOfWords;
    }

    public List<Textable> getWordsContainsOnlyInFirstSentence() {
        // 1 - find words
        // 2 - add all words of first sentence to uniqueWordsOfFirstSentence
        // 3 - review all sentence except first sentence
        // 4 - if first sentence contains word from others sentences delete
        // that word from uniqueWordsOfFirstSentence
        List<Textable> uniqueWordsOfFirstSentence = new ArrayList<>();
        text.getSentenceList().get(0).getSentence().stream()
                .filter(textable -> textable.getText().matches(wordPattern))
                .forEach(uniqueWordsOfFirstSentence::add);
        text.getSentenceList().stream().filter(sentence -> text.getSentenceList().get(0) != sentence)
                .flatMap(sentence -> sentence.getSentence().stream()).forEach(textable -> {
            if (isFirstSentenceContainsWord(textable)) {
                uniqueWordsOfFirstSentence.remove(textable);
            }
        });
        return uniqueWordsOfFirstSentence;
    }

    private boolean isFirstSentenceContainsWord(Textable word) {
        for (Textable i : text.getSentenceList().get(0).getSentence()) {
            if (i.getText().equals(word.getText().toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public List<Textable> getWordsFixedLengthWithoutRepeatInQuestionableSentence(int wordLength) {
        // 1 - find questionable sentences
        // 2 - find words with fixed length and delete identical words
        // 3 - add to sentence if all is ok
        // 4 - add sentence to list of sentences and clear last sentence
        List<Textable> wordsFixedLengthWithoutRepeat = new ArrayList<>();
        text.getSentenceList().stream().filter(sentence ->
                sentence.getSentence().get(sentence.getSentence().size() - 1).getText().equals("?"))
                .flatMap(sentence -> sentence.getSentence().stream())
                .filter(textable -> textable.getText().matches(wordPattern))
                .filter(textable -> textable.getText().length() == wordLength).distinct()
                .forEach(wordsFixedLengthWithoutRepeat::add);
        return wordsFixedLengthWithoutRepeat;
    }

    public List<Sentence> getSentencesWithSwappedLongestAndVowelWords() {
        List<Sentence> sentenceList = new ArrayList<>();
        // copy the elements from the main sentence to temporarySentence
        // for saving sequence words of main sentences
        text.getSentenceList().forEach(sentence -> {
            Sentence temporarySentence = new Sentence();
            sentence.getSentence().forEach(textable ->
                    temporarySentence.getSentence().add(textable.getInstance(textable.getText())));
            Textable firstVowelWord = getWordWithFirstVowel(temporarySentence);
            Textable longestWord = getLongestWord(temporarySentence);
            swapLongestAndFirstVowelWords(temporarySentence, longestWord, firstVowelWord);
            sentenceList.add(temporarySentence);
        });
        return sentenceList;
    }

    private Word getWordWithFirstVowel(Sentence sentence) {
        for (Textable i : sentence.getSentence()) {
            if (i.getText().toLowerCase().matches("[aoiue]+[a-z]?")) {
                return (Word) i;
            }
        }
        return new Word("");
    }

    private Word getLongestWord(Sentence sentence) {
        return (Word) sentence.getSentence().stream().filter(textable -> textable.getText().matches(wordPattern))
                .max(Comparator.comparingInt(a -> a.getText().length())).get();
    }

    private void swapLongestAndFirstVowelWords(Sentence sentence, Textable longest, Textable vowel) {
        sentence.getSentence().set(sentence.getSentence().indexOf(longest), vowel);
        sentence.getSentence().set(sentence.getSentence().indexOf(vowel), longest);
    }

    public List<Textable> getSortedTextByFirstLetter() {
        return text.getSentenceList().stream().flatMap(sentence -> sentence.getSentence().stream())
                .filter(textable -> textable.getText().matches(wordPattern)).sorted().collect(Collectors.toList());
    }

    public List<Textable> getSortedWordsByPercentageVowelLetters() {
        List<Textable> sortedTextByPercentageVowelLetters = new ArrayList<>();
        Map<Textable, Integer> mapOfWordsWithPercentage = getWordsWithPercentageVowelLetters();
        mapOfWordsWithPercentage.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .forEach(wordIntegerEntry -> sortedTextByPercentageVowelLetters.add(wordIntegerEntry.getKey()));
        return sortedTextByPercentageVowelLetters;
    }

    private Map<Textable, Integer> getWordsWithPercentageVowelLetters() {
        Map<Textable, Integer> wordsWithPercentageVowelLetters = new HashMap<>();
        text.getSentenceList().stream().flatMap(sentence -> sentence.getSentence().stream()).filter(textable ->
                textable.getText().matches(wordPattern)).forEach(textable ->
                wordsWithPercentageVowelLetters.put(textable, getPercentageVowelLetters(textable)));
        return wordsWithPercentageVowelLetters;
    }

    private int getPercentageVowelLetters(Textable textable) {
        double numberOfVowelLetters = (double) textable.getText().chars()
                .filter(c -> isVowel((char) c)).count();
        return (int) (numberOfVowelLetters / textable.getText().length() * 100);
    }

    private boolean isVowel(Character character) {
        return character.toString().toLowerCase().matches("[aoiue]");
    }

    public List<Textable> getVowelWordsSortedByConsonant() {
        // 1 - save only words
        // 2 - save only words which begins on vowel letter
        // 3 - save only words which begins on vowel letter and contain consonant letter
        // 4 - sort by consonant letter
        List<Textable> vowelWords = new ArrayList<>();
        text.getSentenceList().stream().flatMap(sentence -> sentence.getSentence().stream())
                .filter(textable -> textable.getText().matches(wordPattern))
                .filter(textable -> isVowel(textable.getText().charAt(0)))
                .filter(this::isContainsConsonant).forEach(vowelWords::add);
        return vowelWords.stream().sorted(Comparator.comparing(o ->
                getConsonantCharacter(o.getText().toLowerCase()))).collect(Collectors.toList());
    }

    private boolean isContainsConsonant(Textable textable) {
        int countConsonant = (int) textable.getText().chars()
                .filter(letter -> !isVowel((char) letter)).count();
        return countConsonant != 0;
    }

    private Character getConsonantCharacter(String word) {
        for (int i = 0; i < word.length(); i++) {
            if (!isVowel(word.charAt(i))) {
                return word.charAt(i);
            }
        }
        return ' ';
    }

    public List<Textable> getWordsSortedByNumberOfFixedLetter(Character fixedLetter) {
        // 1 - filter only words
        // 2 - fill a map [word, number of fixedLetter in the word]
        // 3 - sorted a map first by a value[number of fixed Letter]
        // then sorted by a key[word]
        // 4 - add sorted words to a list[sortedWords]
        Map<Textable, Integer> wordsWithNumberOfFixedLetter = new HashMap<>();
        text.getSentenceList().stream().flatMap(sentence -> sentence.getSentence().stream())
                .filter(textable -> textable.getText().matches(wordPattern))
                .forEach(textable -> wordsWithNumberOfFixedLetter.put(textable,
                        getNumberOfFixedLetter(textable, fixedLetter)));
        List<Textable> sortedWords = new ArrayList<>();
        wordsWithNumberOfFixedLetter.entrySet().stream()
                .sorted(Map.Entry.<Textable, Integer>comparingByValue(Integer::compareTo)
                        .thenComparing(Map.Entry::getKey)).forEach(words -> sortedWords.add(words.getKey()));
        return sortedWords;
    }

    private Integer getNumberOfFixedLetter(Textable word, Character fixedLetter) {
        return (int) word.getText().toLowerCase().chars().filter(c -> c == fixedLetter).count();
    }

    public List<Textable> getSortedFixedListByNumberOfWordsInText(List<String> wordsList) {
        // 1 - create a map [word : number in text] and fill it [word from fixedList : 0]
        // 2 - build a pattern from all words in the fixed list
        // 3 - make all words to lower case
        // 4 - filter words by a pattern
        // 5 - for every remaining word: increment suitable value in a map
        Map<Textable, Integer> wordsWithNumberInText = new HashMap<>();
        wordsList.forEach(textable -> wordsWithNumberInText.put(new Word(textable), 0));
        Pattern fixedListPattern = buildPatternForFixedList(wordsList);
        text.getSentenceList().stream().flatMap(sentence -> sentence.getSentence().stream())
                .map(textable -> textable.getInstance(textable.getText().toLowerCase()))
                .filter(textable -> textable.getText().matches(fixedListPattern.pattern()))
                .forEach(textable -> wordsWithNumberInText.computeIfPresent(textable, (k, v) -> v + 1));
        LinkedList<Textable> sortedWordsList = new LinkedList<>();
        wordsWithNumberInText.entrySet().stream().sorted(Map.Entry.comparingByValue())
                .forEach(textableIntegerEntry -> sortedWordsList.addFirst(textableIntegerEntry.getKey()));
        return sortedWordsList;
    }

    private Pattern buildPatternForFixedList(List<String> wordsList) {
        StringBuilder fixedListRegex = new StringBuilder();
        wordsList.forEach(textable -> fixedListRegex.append(textable.toLowerCase()).append("|"));
        return Pattern.compile(fixedListRegex.toString());
    }

    public List<Sentence> getCuttedSentences(Character startCharacter, Character endCharacter) {
        // 1 - find indexes of start and end words [words contains this characters]
        // 2 - add all words from sentences except words that are between start and end words
        // 3 - cutting start and end words suitable to position
        List<Sentence> listOfCuttedSentences = new ArrayList<>();
        text.getSentenceList().forEach(sentence -> {
            int indexStart = getStartIndexSentenceObject(sentence, startCharacter);
            int indexEnd = getEndIndexSentenceObject(sentence, endCharacter);
            if ((indexStart != -1 && indexEnd != -1) && (indexStart < indexEnd)) {
                Sentence remainingWords = new Sentence();
                for (int i = 0; i <= indexStart; i++) {
                    remainingWords.getSentence().add(sentence.getSentence().get(i));
                }
                cutWordFromStartCharacterToEnd(remainingWords, sentence.getSentence().get(indexStart), startCharacter);
                remainingWords.getSentence().add(sentence.getSentence().get(indexEnd));
                cutWordFromEndCharacterToEnd(remainingWords, sentence.getSentence().get(indexEnd), endCharacter);
                for (int i = indexEnd + 1; i < sentence.getSentence().size(); i++) {
                    remainingWords.getSentence().add(sentence.getSentence().get(i));
                }
                listOfCuttedSentences.add(remainingWords);
            } else {
                listOfCuttedSentences.add(sentence);
            }
        });
        return listOfCuttedSentences;
    }

    private void cutWordFromStartCharacterToEnd(Sentence remainingWords, Textable sentenceObject, Character character) {
        remainingWords.getSentence().set(remainingWords.getSentence().size() - 1,
                sentenceObject.getInstance(
                        sentenceObject.getText().substring(0, sentenceObject.getText().indexOf(character))));
    }

    private void cutWordFromEndCharacterToEnd(Sentence remainingWords, Textable sentenceObject, Character character) {
        remainingWords.getSentence().set(remainingWords.getSentence().size() - 1,
                sentenceObject.getInstance(
                        sentenceObject.getText().substring(sentenceObject.getText().lastIndexOf(character) + 1)));
    }

    private int getStartIndexSentenceObject(Sentence sentence, Character startCharacter) {
        Textable startWord = sentence.getSentence().stream().filter(textable ->
                textable.getText().contains(startCharacter.toString()))
                .findFirst().orElse(null);
        return (startWord != null) ? sentence.getSentence().indexOf(startWord) : -1;
    }

    private int getEndIndexSentenceObject(Sentence sentence, Character endCharacter) {
        Textable endWord = sentence.getSentence().stream().filter(textable ->
                textable.getText().contains(endCharacter.toString()))
                .filter(textable -> textable.getText().contains(endCharacter.toString()))
                .reduce((a, b) -> b).orElse(null);
        return (endWord != null) ? sentence.getSentence().lastIndexOf(endWord) : -1;
    }

    public List<Sentence> getSentencesAfterDeletingFixedConsonantWords(int length) {
        List<Sentence> listAfterDeletingFixedConsonantWords = new ArrayList<>();
        text.getSentenceList().forEach(sentence -> {
            Sentence sentenceAfterDeletingFixedConsonantWords = new Sentence();
            sentenceAfterDeletingFixedConsonantWords.setSentence(
                    sentence.getSentence().stream().filter(textable -> !((textable.getText().length() == length)
                            && !isVowel(textable.getText().charAt(0)))).collect(Collectors.toList()));
            listAfterDeletingFixedConsonantWords.add(sentenceAfterDeletingFixedConsonantWords);
        });
        return listAfterDeletingFixedConsonantWords;
    }

    public List<Textable> getWordsSortedByNumberOfFixedLetterReversed(Character fixedLetter) {
        List<Textable> sortedByNumberOfFixedCharacter =
                new ArrayList<>(getWordsSortedByNumberOfFixedLetter(fixedLetter));
        Collections.reverse(sortedByNumberOfFixedCharacter);
        return sortedByNumberOfFixedCharacter;
    }

    public String getLongestPalindrome() {
        // 1 - transfer all the words of the sentence to one line
        // 2 - check every as long as possible substring by the formula [i - lowerDigit][i + lowerDigit]
        // lowerDigit determines how many elements remain to the start or to the end
        // 3 - if palindrome and length is bigger than max palindrome then change
        StringBuilder longestPalindrome = new StringBuilder();
        StringBuilder allWordsFromSentence = new StringBuilder();
        text.getSentenceList().stream().flatMap(sentence -> sentence.getSentence().stream())
                .filter(textable -> textable.getText().matches(wordPattern)).forEach(textable -> {
            allWordsFromSentence.append(textable.getText()).append(" ");
            for (int i = 1; i < allWordsFromSentence.toString().length() - 1; i++) {
                int lowerDigit = getLowerDigit(i, allWordsFromSentence.toString().length() - i);
                String palindrome = getPalindrome(allWordsFromSentence.substring(i - lowerDigit, i + lowerDigit));
                if (palindrome.length() > longestPalindrome.length()) {
                    longestPalindrome.delete(0, longestPalindrome.length());
                    longestPalindrome.append(palindrome);
                }
            }
        });
        return longestPalindrome.toString();
    }

    private int getLowerDigit(int a, int b) {
        return a > b ? b : a;
    }

    private String getPalindrome(String string) {
        // 1 - take the centre of the string
        // 2 - create a left and right position
        // 3 - if left == right move a left to position to a start of the string
        // and right to the end on one position
        // 4 - if left != right return max palindrome
        String palindrome = "";
        int i = string.length() / 2;
        int left = i - 1;
        int right = i + 1;
        while (left >= 0 && right < string.length()) {
            if (string.charAt(left) == string.charAt(right)) {
                palindrome = string.substring(left, right + 1);
                left--;
                right++;
            } else {
                break;
            }
        }
        return palindrome;
    }

    public List<Textable> modifyWordsByDeletingLetters() {
        // 1 - create a list of all words
        // 2 - delete all characters [first character of the current word] in the next words
        // 3 - delete all characters [last character of the current word] in the all previous words
        // 4 - return all not empty words
        List<Textable> allWords = new ArrayList<>();
        text.getSentenceList().stream().flatMap(sentence -> sentence.getSentence().stream())
                .filter(textable -> textable.getText().matches(wordPattern)).forEach(allWords::add);
        for (int i = 0; i < allWords.size() - 1; i++) {
            if (!allWords.get(i).getText().equals("")) {
                deleteFixedLetterOnInterval(allWords, i + 1, allWords.size(),
                        allWords.get(i).getText().charAt(0));
                deleteFixedLetterOnInterval(allWords, 0, i - 1,
                        allWords.get(i).getText().charAt(allWords.get(i).getText().length() - 1));
            }
        }
        return allWords.stream().filter(textable -> textable.getText().matches(wordPattern))
                .collect(Collectors.toList());
    }

    private void deleteFixedLetterOnInterval(List<Textable> allWords, int startInterval,
                                             int endInterval, Character character) {
        for (int i = startInterval; i < endInterval; i++) {
            StringBuilder wordWithoutFixedLetter = new StringBuilder();
            allWords.get(i).getText().chars()
                    .filter(c -> (char) c != character).forEach(c -> wordWithoutFixedLetter.append((char) c));
            allWords.set(i, new Word(wordWithoutFixedLetter.toString()));
        }
    }

    public Sentence getChangedFixedLengthToFixedString(int indexSentence, int length, String substring) {
        Sentence changedSentence = new Sentence();
        if (indexSentence >= 0 && indexSentence < text.getSentenceList().size()) {
            text.getSentenceList().get(indexSentence).getSentence().stream()
                    .map(textable -> (textable.getText().length() == length)
                            ? textable = textable.getInstance(substring)
                            : textable.getInstance(textable.getText()))
                    .forEach(textable -> changedSentence.getSentence().add(textable));
        }
        return changedSentence;
    }

    public Text getText() {
        return text;
    }
}


